LATEX_COMPILATIONS=2

# Org files (hopefully) sorted depending on their content
SRC=$(wildcard *.org)
ORG_DEP_FILES=$(patsubst %.org, %.deps, $(SRC))
SLIDES=$(shell grep -l beamer *.org)
WEBPAGES=$(shell grep -l html-postamble *.org)
DOCUMENTS=$(filter-out $(SLIDES) $(WEBPAGES), $(SRC))

# Figures that can be generated from a .fig
FIGURES=$(addprefix ./, $(patsubst %.fig, %.pdf, $(wildcard *.fig)))

# Diagrams that can be generated from a .pu
DIAGRAMS=$(addprefix ./, $(patsubst %.pu, %.png, $(wildcard *.pu)))

# Do we have a correction to generate
ifneq ($(WEBPAGES),)
HAS_HTML_CORRECTION=$(shell grep :correction: $(WEBPAGES))
endif
ifneq ($(HAS_HTML_CORRECTION),)
HTML_CORRECTION=$(patsubst %.org, correction_%.html, $(WEBPAGES))
CORRECTION_FILES=correction_index.html
endif
ifneq ($(DOCUMENTS),)
CORRECTIONS=$(shell grep -l :correction: $(DOCUMENTS))
endif
ifneq ($(CORRECTIONS),)
OTHER_DOCUMENTS=$(filter-out $(CORRECTIONS), $(DOCUMENTS))
TEX_CORRECTIONS=$(patsubst %.org, Correction_%.tex, $(CORRECTIONS))
TEX_DOCUMENTS=$(patsubst %.org, Sujet_%.tex, $(CORRECTIONS))
TEX_OTHER=$(patsubst %.org, %.tex, $(OTHER_DOCUMENTS))
else
TEX_OTHER=$(patsubst %.org, %.tex, $(DOCUMENTS))
endif

# Various targets we want to build
TEX_SLIDES=$(patsubst %.org, %.tex, $(SLIDES))
PDF_TARGETS=$(patsubst %.tex, %.pdf, $(TEX_SLIDES) $(TEX_DOCUMENTS) $(TEX_CORRECTIONS) $(TEX_OTHER))
HTML_TARGETS=$(patsubst %.org, %.html, $(WEBPAGES))

# Plantuml integration in org-mode
PLANTUML_ARCHIVE=$(shell ls $$(dirname $$(realpath $$(which plantuml)))/../libexec/plantuml.jar)
ifneq ($(PLANTUML_ARCHIVE),)
PLANTUML_JP=(setq org-plantuml-jar-path (expand-file-name \"$(PLANTUML_ARCHIVE)\"))
PLANTUML_LL=(plantuml . t)
endif

.PHONY: default all clean distclean FichiersTP Projet_AMC

default: all

# Files included in org files, automaticly computed
-include $(ORG_DEP_FILES)
-include dependences.mk

# Emacs
# Don't forget to customize org-export-backends to include beamer
# And to install some packages : ess, plantuml-mode
EMACS_COMMAND=emacs
EMACS=$(EMACS_COMMAND) $(EMACS_OPTIONS) --eval "(progn \
	(setq enable-local-eval t) \
	(setq enable-local-variables t) \
	(setq org-export-babel-evaluate t) \
	(setq org-confirm-babel-evaluate nil) \
	(setq org-latex-listings 'minted) \
	(setq org-latex-with-hyperref nil) \
	$(PLANTUML_JP) \
  	(org-babel-do-load-languages 'org-babel-load-languages '((R . t) $(PLANTUML_LL) (shell . t))) \
	(setq org-safe-remote-resources '(\"https://fniessen.github.io/org-html-themes/org/theme-readtheorg.setup\")) \
	)"
EXCLUDE_CORRECTION=--eval "(setq org-export-with-drawers '(not \"correction\"))"
EXCLUDE_SUJET=--eval "(progn \
	(setq org-export-with-drawers '(not \"sujet\")) \
	(setq org-export-exclude-tags '(\"sujet\")) \
	)"

# Files for practical lab
DEPS=$(ORG_DEPS) $(ORG_FILE_DEPS) $(EXTRA_DEPS)
TP_FILES=$(HTML_TARGETS) $(DIAGRAMS) $(DEPS)
CORRECTION_FILES += $(shell find . -name "*.java")

all: $(PDF_TARGETS) $(HTML_TARGETS)

debug:
	@echo "Sources : $(SRC)"
	@echo "Figures : $(FIGURES)"
	@echo "Diagrams : $(DIAGRAMS)"
	@echo "ORG_DEPS = $(ORG_DEPS)"
	@echo "ORG_FILE_DEPS = $(ORG_FILE_DEPS)"
	@echo "PLANTUML : jar=$(PLANTUML_ARCHIVE), path=$(PLANTUML_JP), language=$(PLANTUML_LL)"
	@echo "Tex : slides=$(TEX_SLIDES), documents=$(TEX_DOCUMENTS)"
	@echo "Pdf targets : $(PDF_TARGETS)"
	@echo "Html targets : $(HTML_TARGETS)"
	@echo "Html correction : $(HTML_CORRECTION)"
	@echo "Tex with corrections : $(TEX_CORRECTIONS)"
	@echo "Tex without correction : $(OTHER_DOCUMENTS)"
	@echo "TP files : $(TP_FILES)"
	@echo "Correction files : $(CORRECTION_FILES)"

programs: $(PROGRAMS)

$(ORG_DEP_FILES): %.deps: %.org
	echo 'ORG_FILE_DEPS+='$$($(EMACS) --batch $< --eval "(mapc 'print (org-element-map (org-element-parse-buffer) 'link (lambda (link) (when (string= (org-element-property :type link) \"file\") (org-element-property :path link)))))" | tr -d '"') >$@
	echo 'ORG_DEPS+='$$($(EMACS) --batch $< --eval "(mapc 'print (org-element-map (org-element-parse-buffer) 'keyword (lambda (keyword) (when (string= (org-element-property :key keyword) \"INCLUDE\") (org-element-property :value keyword)))))" | cut -d' ' -f 1 | tr -d '"\') >>$@

$(TEX_SLIDES): %.tex: %.org $(ORG_DEPS)
	$(EMACS) --batch $< --funcall org-beamer-export-to-latex

$(TEX_OTHER): %.tex: %.org $(ORG_DEPS)
	$(EMACS) --batch $< --eval "(org-export-to-file 'latex \"$@\" )" --funcall org-latex-export-to-latex

$(TEX_DOCUMENTS): Sujet_%.tex: %.org $(ORG_DEPS)
	$(EMACS) --batch $< $(EXCLUDE_CORRECTION) --eval "(org-export-to-file 'latex \"$@\" )" --funcall org-latex-export-to-latex

# HTML targets are not computed in batch mode because the syntax colorization using
# htmlize does not work in batch mode.
$(HTML_TARGETS): %.html: %.org $(ORG_DEPS)
	$(EMACS) $< $(EXCLUDE_CORRECTION) --funcall org-html-export-to-html --funcall save-buffers-kill-terminal

$(HTML_CORRECTION): correction_%.html: %.org $(ORG_DEPS)
	$(EMACS) $< $(EXCLUDE_SUJET) --eval "(org-export-to-file 'html \"$@\" )" --funcall org-html-export-to-html --funcall save-buffers-kill-terminal

$(TEX_CORRECTIONS): Correction_%.tex: %.org $(ORG_DEPS)
	$(EMACS) $< $(EXCLUDE_SUJET) --batch --eval "(org-export-to-file 'latex \"$@\" )" --funcall org-latex-export-to-latex

%.tangle: %.org
	$(EMACS) --batch $< --funcall org-babel-tangle

$(PDF_TARGETS): %.pdf: %.tex $(ORG_FILE_DEPS)
	for num in $$(seq $(LATEX_COMPILATIONS));do \
		pdflatex -shell-escape $*.tex;\
		num=$$(expr $$num - 1);\
	done

$(FIGURES): %.pdf: %.fig
	fig2dev -Lpdf $^ >$@

define PLANTUML_PROLOGUE
skinparam dpi 300
hide empty members
skinparam shadowing false
skinparam classAttributeIconSize 0
hide circle
endef
export PLANTUML_PROLOGUE
$(DIAGRAMS): %.png: %.pu
	echo "$$PLANTUML_PROLOGUE" | cat - $< | java -Djava.awt.headless=true -jar $(PLANTUML_ARCHIVE) -tpng -p -charset UTF-8 >$@

FichiersTP: $(TP_FILES)
	@echo $^

Projet_AMC:
	if [ ! -d $@ ];then mkdir $@;fi
	cp -r $(TEX_DOCUMENTS) img src $@

Correction.zip: $(CORRECTION_FILES)
	if [ -n "$^" ];then zip -r $@ $^;else echo "*** No correction ***";fi

clean:
	-rm -rf *.aux *.bbl *.blg *.log *.out *.snm *.nav *.toc *.vrb *~ \#*\# *.bak *.o *.class _minted*

distclean: clean
	-rm -f $(TEX_SLIDES) $(TEX_DOCUMENTS) $(PDF_TARGETS) $(HTML_TARGETS)

REF_MF=$(HOME)/ownCloud/Moulinettes/Makefile
update:
	if ! diff -q Makefile $(REF_MF) >/dev/null;then cp -v $(REF_MF) .;$(MAKE) distclean;fi

reverse-update:
	if ! diff -q Makefile $(REF_MF) >/dev/null;then cp -v Makefile $(REF_MF);fi
