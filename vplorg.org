#+LABEL: doc_vplorg
* vplorg : Développement d'une activité et exécution locale

Cette documentation présente une organisation et un ensemble de scripts permettant l'exécution
locale et l'/upload/ d'une activité fondée sur l'activité de programmation ='iVPL'=.
Toutes les ressources présentées ici se trouvent dans le dépôt de iVPL, accessible à l'URL
[[https://gricad-gitlab.univ-grenoble-alpes.fr/projet/ivpl]]. Il est possible d'y
trouver :
- dans le répertoire =bin= : le script =vplorg= et les scripts =vplexec= et
  =vpladmin= utilisés par =vplorg= ;
- dans les répertoires =introduction= et =examples= : des exemples d'utilisation
  du format présenté ici. Attention, démarrer de préférence par =introduction= qui présente plus de fonctionnalités d'iVPL sous la forme d'exemple plus concis et plus simples. Le répertoire =examples= contient des exemples fonctionnels mais plus anciens, n'exploitant pas toutes les facilités offertes par iVPL.
Le principal script présenté ici, =vplorg=, attend que l'activité soit décrite
sous une forme particulière utilisant la syntaxe du =org-mode= d'emacs afin de générer les fichiers
de l'activité, les exécuter et les /uploader/. Si l'activité ='iVPL'= ne vous
dit absolument rien, il vous est conseillé de démarrer par la section documentant iVPL.

*ATTENTION*, pour utiliser =vplorg=, vous aurez besoin d'avoir :
- emacs (au moins 26.10 mais peut-être plus, tout est testé en 28.2)
- bash 4 minimum
- les outils nécessaires pour l'exécution de votre VPL

** Hello world

Ce premier exemple présente une activité nommée =Hello= permettant de demander à des étudiants
d'écrire un programme qui affiche ="Hello world !"=.
#+INCLUDE: "introduction/Hello.org" src org

Une fois cette activité écrite et stockée dans un fichier nommé =Hello.org=, il suffit de télécharger le
contenu du dépôt indiqué en préambule, d'ajouter l'emplacement de son répertoire =bin= à votre =PATH= et de définir la valeur de la variable d'environnement =VPLADMIN_TOKEN= tel qu'indiqué dans la documentation de =vpladmin=.
Vous disposez alors de la commande =vplorg= permettant de :
- lancer localement l'activité comme le ferait un étudiant avec les boutons ='Run'=, ='Debug'= ou
  ='Evaluate'= :
  #+BEGIN_SRC shell :exports code
  vplorg run Hello
  #+END_SRC
  ou
  #+BEGIN_SRC shell :exports code
  vplorg debug Hello
  #+END_SRC
  ou encore
  #+BEGIN_SRC shell :exports code
  vplorg evaluate Hello
  #+END_SRC
  Toutes ces commandes d'exécution locale exécutent l'activité sur les fichiers
  requis (=rf= décrit dans la section suivante). Pour exécuter localement sur
  la correction (=cf= et =hf=), il faudra ajouter 'solution après l'action
  souhaitée, par exemple :
  #+BEGIN_SRC shell :exports code
  vplorg evaluate solution Hello
  #+END_SRC
- /uploader/ les fichiers de l'activité, ainsi que sa description :
  #+BEGIN_SRC shell :exports code
  vplorg push Hello
  #+END_SRC
- evaluer chaque test d'une suite de tests au format VPL (dans =ef/vpl_evaluate_tests=) :
  #+BEGIN_SRC shell :exports code
  vplorg check Hello
  #+END_SRC
- générer un squelette de fichier d'activité pour =vplorg=
  #+BEGIN_SRC shell :exports code
  vplorg template Nom_de_l_Activite
  #+END_SRC


** Les fichiers générés

L'ensemble des scripts d'iVPL travaillent sur une activité de nom =X= en consultant un fichier
nommé =X.org= permettant de générer tous les fichiers attendus par l'activité
='iVPL'=. Le fichier =X.org= doit donc contenir des blocs de code dotés
d'arguments =:tangle= permettant d'indiquer le nom du fichier dans lequel ils doivent être écrits.
Tous ces fichiers doivent être écrits dans un sous-répertoire du répertoire =X= dépendant de leur
fonction :
- =ef= : fichiers necessaires à l'exécution et à l'évaluation de l'activité. Ce sous-répertoire
  concerne entre autres =vpl_evaluate.cases= et =vpl_custom_setup.sh= ;
- =rf= : fichiers fournis comme base aux étudiants, typiquement des fichiers à trous à compléter ;
- =cf= : fichiers faisant partie de la correction écrite par l'enseignant, non fournie aux
  étudiants, consultable sur caséine par les enseignants. Pour chaque fichiers de =cf=, si un
  fichier de =rf= de même nom n'existe pas, les scripts de iVPL en génèrent un vide (base de
  travail vierge pour l'étudiant) ;
- =hf= : similaire à =cf= mais ne donne pas lieu à une génération de fichier dans =rf=.
Si une partie des fichiers servant à l'évaluation doit être fournie aux
étudiants, afin que ceux-ci puissent les utiliser pour leurs propres tests, ils
peuvent être placés à la fois dans =ef= et =rf= : de cette manière l'étudiant
peut les utiliser librement mais leur contenu est écrasé par le contenu fourni
par l'enseignant lors de l'évaluation. Tous les fichiers présents dans =ef= sont
automatiquement ajoutés par iVPL aux 'Files to keep when running' de VPL :
il ne faut donc pas oublier d'effacer les fichiers sensibles à l'aide des
fonctions =read_and_remove= et =get_files= fournies par iVPL (voir la doc de ce
dernier pour plus de détails).
  
Les propriétés données en début du fichier d'exemple =Hello.org= correspondent au comportement
généralement souhaité :
- =:eval never= : les scripts de iVPL évaluent les blocs de code d'un fichier =X.org=.
  Ceci ne concerne pas les fichiers de l'activité qui ne sont donc par défaut jamais évaluables ;
- =:mkdirp yes= : la génération des fichiers de l'activité doit créer les répertoires intermédiaires
  au besoin ;
- =:exports none= : les fichiers de l'activité de font pas partie de la description de celle-ci.

** L'exécution locale

Lors de l'exécution ou l'évaluation d'une activité =X=, une fois les fichiers générés à partir de
=X.org=, l'exécution proprement dite se déroule dans le répertoire =X_run=. Contrairement à ce
que fait le module VPL, cette exécution locale se déroule sans isolation (pas de =chroot= ou de
limites sur les resources), elle ne remplace donc pas une exécution en conditions réelles sur le
serveur mais permet de tester son activité durant la phase de développement.

L'exécution locale supporte plusieurs mots clés, qu'il est possible d'ajouter
après la commande d'exécution :
- =solution= : copie dans le répertoire d'exécution les fichiers de =cf= et
  =hf=, en écrasant éventuellement les fichiers de =rf= ou =ef= déjà présent ;
- le nom d'un répertoire : copie les fichiers de ce répertoire dans le
  répertoire d'exécution (typiquement utile pour tester le code d'un étudiant) ;
- le nom d'un fichier =.zip= : extrait le contenu de ce fichier dans le
  répertoire d'exécution (même usage que le nom de répertoire) ;
- dans tous les autres cas, le mot clé est considéré comme le nom d'un mode
  supporté par iVPL (voir documentation d'iVPL). Dans ce cas,
  le mode est activé pour l'exécution. C'est le cas typiquement des modes
  =debug= (sorties plus verbeuses pour iVPL) ou =keep_files= (non suppression
  des fichiers temporaires).
Voici quelques exemples de commandes généralement utiles :
- information maximale, pour debugger une solution en train d'être écrite :
  #+BEGIN_SRC shell :exports code
  vplorg evaluate debug keep_files solution Hello
  #+END_SRC
- exécution des fichiers fournis pour vérifier qu'ils compilent (le cas échéant)
  mais ne passent aucun test :  
  #+BEGIN_SRC shell :exports code
  vplorg evaluate Hello
  #+END_SRC
- exécution des fichiers présents dans le répertoire Tartempion (solution écrite
  par un étudiant) :
  #+BEGIN_SRC shell :exports code
  vplorg evaluate debug Tartempion Hello
  #+END_SRC
- exécution des fichiers présents dans l'archive zip =Submission.zip= (solution écrite
  par un étudiant) :
  #+BEGIN_SRC shell :exports code
  vplorg evaluate debug Submission.zip Hello
  #+END_SRC

Si votre terminal le supporte, l'exécution locale se fait en couleurs, en :
- noir : affichages visibles par l'étudiant ;
- bleu : titres visibles par les étudiants ;
- jaune : affichages toujours actifs, cachés aux étudiants ;
- vert : affichages du mode debug, cachés aux étudiants ;
- rouge : affichages des avertissements et erreurs, dont le détail est caché aux étudiant.

** L'/upload/

Les scripts de iVPL sont capables d'envoyer sur le serveur tous les fichiers de
l'activité et de changer certains réglages du VPL correspondant. Cela se fait
grâce à l'action =push= de la commande =vplorg=. Pour
cela il faut que le fichier =X.org= d'une activité =X= génère le fichier
=X/remote_settings.sh= qui devra lui même fixer la variables =vplid= au numéro
de VPL sur le serveur. L'activité devra tout de même
être créée manuellement au préalable.

Les réglage utilisables pour le VPL sont documentés ici (dans la partie 'champs
bruts de la base de données') :\\
[[https://moodle.caseine.org/mod/page/view.php?id=61502][https://moodle.caseine.org/mod/page/view.php?id=61502]]
Il sont toujours de la forme :
#+BEGIN_SRC shell :exports code
settings[nom_du_setting]=valeur
#+END_SRC
Le script =remote_settings.sh= est exécuté avant un =push=, vous pouvez donc y
placer toute action pertinente à entreprendre (en =bash=) avant l'envoi des fichiers.

La description de l'activité est générée au format html à partir du fichier =X.org= lors de
l'utilisation de la commande =vplorg=. Elle est envoyée sur le serveur lors du push. Elle se trouve
dans le fichier =intro.html= du répertoire =X= et peut donc être consultée pour vérification avant
d'effectuer un =push=.

** Organiser son activité

Le format =org= permet d'organiser son texte à l'aide d'une syntaxe simple similaire à celle de
=markdown=. En particulier, il est possible de manipuler des listes numérotées ou non, de découper son
texte en sections, sous-sections, sous-sous-sections, etc. Le document peut ensuite être exporté vers
plusieurs formats classiques (=latex=, =html=, ...).  Présenter toutes les fonctionnalités d'
=org= n'est pas le but de cette documentation et le lecteur curieux est invité à se référer à
la documentation officielle : [[https://orgmode.org/manual]]

Le but de cette section est essentiellement de donner des pistes afin de découper son activité en
exercices décrits par des parties indépendantes du fichier au format =org= et d'éviter la
duplication de certaines parties du texte. Cela repose essentiellement sur les idées suivantes :
- le dictionnaire résultant de l'évaluation de =vpl_evaluate.cases= peut être en partie
  construit de manière programmatique ;
- le fichier =vpl_evaluate.cases= est un code en =python= qu'il est possible de découper en plusieurs
  blocs de code concaténés les uns aux autres à l'aide de l'argument =:output append= (comportement
  par défaut mais le demander explicitement ne fait pas de mal) ;
- =org= mode supporte la syntaxe =noweb= pour manipuler des blocs de code. En particulier :
  - une activité est nommée à l'aide de la propriété =#+NAME= ;
  - un bloc de code ayant pour argument =:noweb yes= peut inclure un bloc de code nommé s'il
    contient, seule sur une ligne, une référence de la forme suivante : =<<nom>>=
- il est possible, à l'aide de tags, de ne pas inclure certaines sections dans l'export de la
  description. Cela permet de profiter du /folding/ d'=emacs= pour toutes les parties du fichier de
  description de l'activité.
Tout ceci est illustré par l'exemple =Hello= étendu avec un cas de test en français :
#+INCLUDE: "introduction/Hello_elabore.org" src org
Ce fichier permet de générer la description en html de l'activité ainsi que les fichiers suivants :
- dans =Hello_elabore/ef=, =vpl_evaluate.cases= :
  #+BEGIN_SRC python :export code
# On initialise un dictionnaire vide
test_cases = {}

# Ajout du premier cas de test
test_cases['hello_world.sh'] = {
    'output': "Hello world !",
    'tests': {
        'test_1': {
            'show': 1
        },
        'test_2': {
            'output': "HELLO WORLD !",
            'output_postprocessing': "tr 'a-z' 'A-Z'"
        }
    }
}

solution = r"""
echo "Salut monde !"
"""

# Ajout du second cas de test
test_cases['salut_monde.sh'] = {
    'solution': solution,
    'tests': {
        'test_1': {
        },
    }
}

# suppression d'un ou plusieurs cas de test, utile lors du développement de l'activité
# del cases['hello_world.sh']
# del cases['salut_monde.sh']
  #+END_SRC
- dans =Hello_elabore/cf=, =hello_world.sh= :
  #+BEGIN_SRC shell :export code
  echo "Hello world !"
  #+END_SRC
- dans =Hello_elabore/cf=, =salut_monde.sh= :
  #+BEGIN_SRC shell :export code
  echo "Salut monde !"
  #+END_SRC

** Fichiers externes

Il n'est pas toujours possible d'inclure l'intégralité d'une activité dans un seul fichier,
quand celle-ci est trop grosse ou quand elle dépend de fichiers provenants de plusieurs projets
logiciels distincts. Il est alors possible de fournir les fichiers de l'activité =X= dans un
répertoire =X_files=. A la différence du répertoire =X=, le répertoire =X_files= n'est pas effacé
et recrée par =vplorg=, on peut donc y placer des fichiers élaborés à la main.
Le répertoire =X_files= doit suivre la
même organisation que le répertoire =X= généré par =X.org=, autrement dit, ses fichiers doivent se
trouver dans les bons sous répertoires =ef=, =cf=, =rf=, =hf=.

** Blocs de code

Le mode =org= inclut la possibilité d'exécuter un bloc de code contenu dans =X.org=. Dans ce cas,
ce bloc de code devra être exécutable (argument =:eval yes=). Cette exécution est lancée après la génération
des autres fichiers (description de l'activité en =html= et blocs de code avec =tangle=), elle peut donc
être utilisée pour appliquer des post traitements aux fichiers générés.

A titre d'exemple, dans l'activité =Hello=, plutôt que d'include la solution du cas de test
="hello_world.sh"= dans =Hello.org=, celle ci aurait pu être récupérée et copiée vers =Hello=
de la manière suivante :
#+BEGIN_SRC org :exports code
,#+BEGIN_SRC shell :eval yes
mkdir -p Hello/cf
cp Chemin_vers_mes_fichiers_solution/hello_world.sh Hello/cf
,#+END_SRC
#+END_SRC

** La files\under{}table

Si le fichier =X.org= de l'activité =X= contient une table nommée =files_table=,
cette table sera utilisée comme une description des fichiers utilisés par
l'activité et de leur destination. Cette table est organisée de la manière
suivante :
- chaque colonne correspond à une catégorie définie librement par l'utilisateur ;
- la première ligne de chaque colonne correspond à la définition d'une
  catégorie, on y trouve, séparés par le caractère =:=, le nom de la catégorie
  et l'ensemble des répertoires destination pour les fichiers de cette
  catégorie (séparés par des espaces) ;
- chaque ligne suivante contribue à la liste des fichiers appartenant à chacune
  des catégories.
La sémantique générale est la suivante : pour chacune des catégories, tous les
fichiers faisant partie de la catégorie seront copiés vers chacune des
destinations de la catégorie.

Le processus de copie des fichiers décrits par la =files_table= est influencé
par plusieurs variables dont il est possible de fixer la valeur dans le fichier
=local_settings.sh=. Ces variables sont toutes des tables d'association qui
acceptent des valeurs par défaut définies soit en donnant une valeur à la table
elle-même (ce qui correspond à l'entrée =0=), dans les cas de =src_prefix= et
=remove_prefix=, soit en donnant une valuer aux clés =src= et =dst=, dans le cas
de =transform=. Les valeurs par défaut, si elles sont données, s'appliquent à
toutes les clés non définies. Les variables prises en compte sont :
- =src_prefix= : contient, pour chaque catégorie, un nom de
  répertoire dans lequel les fichiers de la catégorie doivent être recherchés.
  La valeur pas défaut de cette table est =src= si le répertoire =src= existe
  dans le répertoire courant et =.= sinon ;
- =remove_prefix= : contient, pour chaque destination, un
  préfixe à supprimer du nom des fichiers copiés vers cette destination.
- =transform= : contient, pour chaque source (catégories définies) ou destination
  (les répertoire vers lesquels copier les fichiers), une expression =bash= (à
  protéger) dépendant de la variable =$filename= (nom de fichier dans la
  =files_table=) qui, lorsqu'elle sera évaluée, devra correspondre respectivement
  au nom du fichier source ou destination à utiliser pour =$filename=. Cette
  variable est la plus générale des trois, =src_prefix= et =remove_prefix= sont
  exprimées en interne dans =vplorg= par les valeurs de =tranform= suivantes :
  - ~src_prefix[source]=Repertoire~ équivaut à ~transform[source]='Repertoire/$filename'~
  - ~remove_prefix[destination]=Prefixe~ équivaut à ~transform[destination]='${filename##Prefixe}'~
Ces variables, ainsi que l'ensemble des fichiers de chaque catégorie (variable
=files_table= avec, pour chaque catégorie, une valeur correspondant à l'ensemble
des fichiers séparés par des espaces) sont utilisables dans tout bloc de code
(shell) exécutable d'une activité =X.org=. En outre, pour chaque catégorie, il
existera une variable de même nom dans le fichier =vpl_evaluate.cases= dont la
valeur est la liste des noms de fichier de la catégorie.
  
A titre d'exemple, la =files_table= et les =local_settings.sh= de l'exemple
d'introduction =C_fonction.org= sont les suivants :
#+BEGIN_SRC org
#+NAME: files_table
| provided: ef rf | required: rf | correction: ef hf     |
|-----------------+--------------+-----------------------|
| main.c          | fonction.c   | correction_fonction.c |
| fonction.h      |              |                       |
| Makefile        |              |                       |

# Réglages locaux utilisés lors de la mise en place des fichiers du VPL
# ici, on dit juste que la partie correction ne garde pas le préfixe correction_
,#+begin_src shell :tangle C_fonction/local_settings.sh
remove_prefix[hf]="correction_"
,#+end_src
#+END_SRC
Ce qui signifie que l'utilisateur a défini 3 catégories :
- =provided= : qui contient 3 fichiers, destinés à être copiés dans =rf= (pour
  être disponibles pour l'utilisateur) et dans =ef= (pour être écrasés par leur
  valeur par défaut lors de l'évaluation) ;
- =required= : qui contient 1 fichier destiné à être fourni à l'utilisateur et
  complété par celui-ci (fichier à modifier de l'exercice copié dans =rf=) ;
- =correction= : qui contient 1 fichier qui sera copié tel quel dans =ef=
  (fichier source utilisé lors de l'évaluation pour compiler la solution) et en
  supprimant le préfixe =correction_= dans =hf= (correction fournie à
  l'enseignant, qui doit avoir le même nom que le fichier fourni aux étudiants).

La =files_table= a été conçue avec plusieurs objectifs en tête :
- permettre à l'enseignant de créer un iVPL à partir d'une base de code existante,
  sans avoir besoin de retoucher aux fichiers déjà écrits ;
- permettre de manipuler des listes de fichiers impliqués dans
  =vpl_evaluate.cases= sans avoir à écrire de code disgracieux. C'est le cas
  dans l'exemple =C_fonction.org=, dans lequel on trouve l'appel à =get_files= :
  #+BEGIN_SRC python
  "files": get_files(required=["main.c"], solution=correction)
  #+END_SRC
  Nous pouvons constater que la liste =required= est donnée explicitement (pas
  de catégorie), mais que la liste =solution= est affectée à la liste
  correspondant à la catégorie =correction= ;
- profiter des capacité d'édition de tables en ascii en orgmode dans Emacs.
La =files_table= rend un peu obsolète le répertoire =X_files=, même si ce
dernier reste supporté.
