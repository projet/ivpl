#!/usr/bin/env python3
import unittest
import vpl_evaluate
from vpl_evaluate import *

testerInstance = None

class MockingTestCase(unittest.TestCase):
	def __init__(self, arg):
		super().__init__(arg)
		self.globally_replaced = {}
		global testerInstance
		testerInstance = self
		vpl_evaluate.mode = {"local": 1, "debug": 1}
		evaluate_file("vpl_evaluate.cases")

	def setUp(self):
		global failures_signal, failures_code
		failures_signal = []
		failures_code = []

	def replace(self, name, replacement):
		dest = vpl_evaluate.__dict__
		source = self.__class__.__dict__
		self.globally_replaced[name] = dest[name]
		dest[name] = source[replacement]

	def restore(self, name):
		dest = vpl_evaluate.__dict__
		dest[name] = self.globally_replaced[name]

	def mock_perform_tests(name, case):
		comment("*** Test {} found ***".format(name))
		return 0

	def mock_run_special(test_name, name, content, test, has_limits):
		global failures_signal, failures_code
		if name in failures_signal:
			message = "failed with signal 15"
			result = {'failure': True, 'signal': 15, 'code': 0}
		elif name in failures_code:
			message = "failed with code 1"
			result = {'failure': True, 'signal': 0, 'code': 1}
		else:
			message = "success"
			result = {'failure': False, 'signal': 0, 'code': 0}
		debug("mock_run_special ({}) when running".format(message), name, "for", test_name, ":")
		for part in ['output', 'error']:
			if test.get(part):
				result[part] = test[part]
			else:
				result[part] = ''
		return result

	def perform_test_cases_with_mock(self, test, original, replacement):
		self.replace(original, replacement)
		debug("MOCKING TEST:", test)
		perform_test_cases()
		self.restore(original)

	def test_enumerate_tests(self):
		self.perform_test_cases_with_mock("enumerate tests", "perform_tests", "mock_perform_tests")

	def test_run_special_success(self):
		self.perform_test_cases_with_mock("run with success", "run_special", "mock_run_special")

	def test_run_special_signal(self):
		global failures_signal
		failures_signal = [".setup", ".validator", "essai"]
		self.perform_test_cases_with_mock("run with signal", "run_special", "mock_run_special")

	def test_run_special_code(self):
		global failures_code
		failures_code = [".setup", ".validator", "essai"]
		self.perform_test_cases_with_mock("run with positive exit code", "run_special", "mock_run_special")

if __name__ == '__main__':
	unittest.main()
