  import java.util.Scanner;

  class ExerciceCut {
      public static void handle(String line, int from, int to) {
          String [] fields = line.split(" ");
          for (int i=from; (i<fields.length) && (i<to); i++)
              System.out.print(fields[i]+" ");
          if ((to < fields.length) && (from <= to))
              System.out.println(fields[to]);
      }

      public static void main(String [] args) {
          int from = Integer.parseInt(args[0]);
          int to = Integer.parseInt(args[1]);
          from--;
          to--;

          Scanner s = new Scanner(System.in);
          while (s.hasNextLine()) {
              String line = s.nextLine();
              handle(line, from, to);
          }
      }
  }
