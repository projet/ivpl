#!/usr/bin/env python3
import unittest
import vpl_evaluate
from vpl_evaluate import *


class RunTestCase(unittest.TestCase):
	def __init__(self, args):
		super().__init__(args)
		vpl_evaluate.mode = {"local": 1, "debug": 1}
		evaluate_file("vpl_evaluate.cases")

	@staticmethod
	def run_shell_content(content, context):
		with open("my_command", 'w') as f:
			f.write("#!/bin/sh\n" + content + "\n")
		data = run_command(content, "my_command", context, 1)
		hidden("Context :")
		pp(context)
		hidden("Result :")
		pp(data)
		return data

	@staticmethod
	def run_special_and_print(test_name, name, content, test, has_limits):
		data = run_special(test_name, name, content, test, has_limits)
		hidden("Content :")
		pp(content)
		hidden("Test :")
		pp(test)
		hidden("Result :")
		pp(data)
		return data

	def test_simple_runs(self):
		data = RunTestCase.run_shell_content("ls", {})
		self.assertFalse(data['failure'])
		context = {'input': "Bonjour à tous,\nJe suis Guillaume\n\nVoilà\n"}
		data = RunTestCase.run_shell_content("cat", context)
		self.assertFalse(data['failure'])
		self.assertEqual(context['input'], data['output'])

	def test_invalid_command(self):
		data = RunTestCase.run_shell_content("MyInvalidCommand", {})
		self.assertTrue(data['failure'])
		self.assertEqual(data['code'], 127)

	def test_various_errors(self):
		data = RunTestCase.run_shell_content("kill -9 $$", {})
		self.assertTrue(data['failure'])
		self.assertEqual(data['signal'], 9)
		data = RunTestCase.run_shell_content("exit 42", {})
		self.assertTrue(data['failure'])
		self.assertEqual(data['code'], 42)
		data = RunTestCase.run_shell_content("sleep 5", {'timeout': 1})
		self.assertTrue(data['failure'])
		self.assertEqual(data['signal'], 9)
		data = RunTestCase.run_shell_content("ls", {'output_limit': 25})
		self.assertFalse(data['failure'])
		self.assertTrue(data['output_bounded'])

	def test_special_success(self):
		data = RunTestCase.run_special_and_print(
			"Success", "my_ls", "#!/bin/sh\necho $PATH\nls\n", {
				'code': 0
			}, 1
		)
		self.assertFalse(data['failure'])
		test = {
			'code': 0, 'output_limit': 25,
			'input': "Bonjour à tous,\nJe suis Guillaume\n\nVoilà\n"
		}
		data = RunTestCase.run_special_and_print(
			"Success", "my_cat", "cat", test, 1
		)
		self.assertFalse(data['failure'])
		self.assertTrue(data['output_bounded'])
		self.assertEqual(test['input'][:test['output_limit']-1], data['output'])

	def test_special_compiled(self):
		data = RunTestCase.run_special_and_print(
			"Compiled", "hello", {
				'filename': "hello.c",
				'content': '#include <stdio.h>\nint main() { printf("Hello\\n"); }',
				'compile': 'gcc -Wall -o $NAME $FILES'
			},{
				'output': 'Hello\n'
			}, 1
		)

if __name__ == '__main__':
	unittest.main()
