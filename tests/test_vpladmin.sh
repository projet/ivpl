#!/usr/bin/env bash

RED=31
GREEN=32

cprint () {
    local color="$1"
    shift
    printf "\e[%dm%s\e[0m" "$color" "$*"
}

print_result () {
    result=$?
    printf "["
    if [ $result -eq 0 ]
    then
        cprint $GREEN SUCCESS
    else
        cprint $RED FAILURE
    fi
    printf "] "
}

token=$VPLADMIN_TOKEN
# path for a local webservice (temporary token)
# export VPLADMIN_PATH="mod/vpl/webservice.php"

vplid=72486

test () {
    local name="$1"
    shift
    local value="$1"
    shift
    vpladmin "$@" | grep ".*<KEY name=\"$name\"><VALUE>$value</VALUE>" >/dev/null
    print_result
}

test_empty_response () {
    vpladmin "$@" | grep -z ".*<RESPONSE>.*</RESPONSE>" >/dev/null
    print_result
}

test_set_and_get () {
    local setting="$1"
    shift
    local value="$1"
    shift
    test success 1 "$@" set_setting "$setting=$value"
    echo "Test setting $setting to $value"
    test value "$value" "$@" get_setting "$setting"
    echo "Checking that $setting value is $value"
}

test_file_push_pull () {
    local filename="$1"
    shift
    test_empty_response "$@" save "$filename"
    echo "Test pushing $filename to the server"
    mv -i "$filename" "${filename}_original"
    test_empty_response "$@" --write-files open
    echo "Test pulling files from the server"
    diff "$filename" "${filename}_original" >/dev/null
    print_result
    echo "Checking that the file content is preserved"
    rm "${filename}_original"
}

original_dir=$(PWD)
dir=$(mktemp -d)
cp "$0" "$dir"
cd "$dir" || exit 1

echo "*** Testing ID transmission"
export VPLADMIN_ID="Invalid"
test_set_and_get maxfiles 5 --id $vplid
export VPLADMIN_ID="$vplid"
test_set_and_get maxfiles 5

echo "*** Testing TOKEN transmission"
export VPLADMIN_TOKEN="Invalid"
test_set_and_get evaluate 1 --token "$token"
export VPLADMIN_TOKEN="$token"
test_set_and_get evaluate 1

echo "*** Testing text file transmission"
cp -i "$0" test_file
test_file_push_pull test_file
rm test_file

echo "*** Testing binary file transmission"
dd if=/dev/random of=test_file count=4096 2>/dev/null
test_file_push_pull test_file --force-binary
rm test_file

echo "*** Testing vpladmin from within the jail"
echo "# FROM JAIL" >test_file
cat "$0" >>test_file
echo "# FROM JAIL" >>test_file
cat >vpl_evaluate.sh <<'METAEND'
# Common script sourced here, but could be sourced in vpl_execution as well
. common_script.sh
cat >vpl_execution <<END
#!/usr/bin/env bash
echo "Content of vpl_environment.sh"
cat vpl_environment.sh
set -x
chmod +x ./vpladmin
./vpladmin --id "$MOODLE_VPL_ID" --token "$MOODLE_USER_TOKEN" --path 'mod/vpl/webservice.php' save test_file
END
chmod +x ./vpl_execution
METAEND
cp "$(which vpladmin)" vpladmin
declare -a files=(test_file vpl_evaluate.sh vpladmin)
test_empty_response save_execution_files "${files[@]}"
echo "Sending files to the server"
test_empty_response set_files_to_keep_when_running "${files[@]}"
echo "Setting files_to_keep_when_running"
mv test_file test_file_original
test_empty_response evaluate
echo "Launching evaluation"
sleep 1
test_empty_response --write-files open
echo "Getting saved file back"
diff test_file test_file_original >/dev/null
print_result
echo "Testing file content"

cd "$(PWD)" || exit 1
rm -r "$dir"
