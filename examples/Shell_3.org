#+OPTIONS: num:nil
#+OPTIONS: toc:nil
#+OPTIONS: html-postamble:nil
#+OPTIONS: html-preamble:nil
#+LANGUAGE: fr
#+EXPORT_EXCLUDE_TAGS: noexport
#+PROPERTY: header-args :eval never :mkdirp yes :exports none

Dans cette partie, comme lors des précédentes activités, pour chaque question, il vous est demandé
de fournir un fichier de commandes (ou script) répondant au problème.

*RAPPEL* : Vous pouvez tester votre solution grâce au bouton ='Run'= qui ouvre un
terminal dans lequel vous pouvez :
- exécuter votre solution grâce à la commande =./exercice_X.sh= où =X= est le numéro de l'exercice
- examiner le répertoire courant pour vérifier que votre solution a bien fait ce qu'il fallait
Le bouton ='Debug'= fait la même chose que ='Run'= mais affiche en plus toutes les commandes exécutée
(précédées d'un caractère ='+'=).

*ATTENTION* : rappelez vous que le bouton ='Evaluate'= évalue tous les exercices pour vous donner une
note. Ne le pressez que lorsque vous avez répondu à toutes les questions.

** Files initialization :noexport:

#+BEGIN_SRC shell :tangle Shell_3/remote_settings.sh
vplid=50595
#+END_SRC

#+BEGIN_SRC python :tangle Shell_3/ef/vpl_evaluate.cases
test_cases = {
      'execute': 'bash -O globasciiranges -- $NAME "$@"'
}
#+END_SRC

** Exercice 1

Ecrivez un script qui, étant donné un argument entier =x=, crée =x= fichiers nommés =fichier1.txt=,
=fichier2.txt= et ainsi de suite jusqu'à =fichierx.txt= et leur donne tous les droits pour
l'utilisateur.

#+BEGIN_SRC python :tangle Shell_3/ef/vpl_evaluate.cases :output append
validateur_1 = r"""num=1
last=$(tail -n 1 .arguments.txt)
while [ "$num" -le "$last" ]
do
    file="fichier$num.txt"
    if [ -f "$file" ]
    then
      if [ ! -r "$file" -o ! -w "$file" -o ! -x "$file" ]
      then
        echo "$file n'a pas tous les droits"
        exit 1;
      fi
    else
      echo "$file n'existe pas"
      exit 2
    fi
    num=$(expr $num + 1)
done
"""

# WARNING : this is only for demonstration, random test sets are unreliable as they
# might both validate and invalidate an incorrect submission depending on the random
# values distribution
from random import randint
test_cases['exercice_1.sh'] = {
          'tests': {
              'bonnie': {'args': [randint(0,2)+2], 'validator': validateur_1, 'show': 1},
              'clyde': {'args': [randint(0,4)+5], 'validator': validateur_1}
          }
}
#+END_SRC

#+BEGIN_SRC shell :tangle Shell_3/cf/exercice_1.sh
#!/bin/bash
num=1
while [ "$num" -le "$1" ]
do
    touch fichier$num.txt
    chmod u+rwx fichier$num.txt
    num=$(expr $num + 1)
done
#+END_SRC

#+BEGIN_SRC shell :tangle Shell_3/wrong/exercice_1.sh
#!/bin/bash
num=1
while [ "$num" -lt "$1" ]
do
    touch fichier$num.txt
    num=$(expr $num + 1)
done
#+END_SRC

** Exercice 2

Ecrivez un script qui, si on lui donne $x$ arguments entiers que nous nommerons $arg_1$, ...,
$arg_x$, affiche tous les produits obtenus avec les préfixes de la séquence d'arguments pris par
ordre croissant. C'est-à-dire, les produits :\\
$arg_1$ \\
$arg_1\times arg_2$ \\
$arg_1\times arg_2\times arg_3$ \\
... \\
$arg_1\times\ldots\times arg_x$

#+BEGIN_SRC python :tangle Shell_3/ef/vpl_evaluate.cases :output append
def generate_args(min, max, arg_range):
    tab = []
    num = randint(min, max)
    for i in range(0, num):
      tab.append(randint(1, arg_range))
    return tab


def prefixes(tab):
    produit = 1
    result = ""
    for val in tab:
      produit = produit * val
      result = result + str(produit) + "\n"
    return result

    
args_2_1 = generate_args(4,4,10)
args_2_2 = generate_args(4,10,100)
args_2_3 = generate_args(4,10,100)
test_cases['exercice_2.sh'] = {
          'tests': {
              'fifi': {'args': args_2_1, 'output': prefixes(args_2_1), 'show': 1},
              'riri': {'args': args_2_2, 'output': prefixes(args_2_2)},
              'loulou': {'args': args_2_3, 'output': prefixes(args_2_3)}
          }
}
#+END_SRC

#+BEGIN_SRC shell :tangle Shell_3/cf/exercice_2.sh
#!/bin/bash
prod=1
for num in $*
do
    prod=$(expr $prod \* $num)
    echo $prod
done
#+END_SRC

#+BEGIN_SRC shell :tangle Shell_3/wrong/exercice_2.sh
#!/bin/bash
prod=1
for num in $*
do
    prod=$(expr $prod + $num)
    echo $prod
done
#+END_SRC

** Exercice 3

Ecrivez un script qui lit un texte sur l'entrée standard (au clavier par défaut) et affiche le
premier mot de la première ligne, les deux premiers mots de la seconde ligne et ainsi de suite
jusqu'aux $x$ premiers mots de la $x^{ième}$ ligne où $x$ est la dernière ligne lue. Si, pour une
ligne donnée, le nombre de mots à afficher et plus grand que le nombre de mots de la ligne, on
affichera simplement tous les mots de la ligne. Les mots lus sont séparés les uns des autres par un ou
plusieurs espaces mais doivent être affiché séparés par un unique espace.

#+BEGIN_SRC python :tangle Shell_3/ef/vpl_evaluate.cases :output append
import re

def lines_3(text):
    num=1
    result=[]
    for line in text.split("\n"):
        line = re.sub(r'^ +', '', line)
        line = re.sub(r' +$', '', line)
        words = re.split(r' +', line)
        separator=""
        new_line=""
        for i in range(0, num):
            if len(words) > i:
                new_line = new_line + separator + words[i]
                separator=" "
        result.append(new_line)
        num = num + 1
    return result


def answer_3(text):
      return "\n".join(lines_3(text))


input_3_1 = "1 2 3 4 5\n1 2 3 4 5\n1 2 3 4 5\n1 2 3 4 5\n1 2 3 4 5\n1 2 3 4 5\n"
input_3_2 = "entree   avec  des\nespaces en   trop\nau milieu\net rien de plus\n"
input_3_3 = "  entree avec des   \nespaces   en trop   \nun    peu partout    \n     dans tout   le  texte,   \n  au    bout  aussi   \n"
input_3_4 = "entree avec\n   des   espaces en\n\n\ntrop, tout comme  des \n\n lignes en trop \n\n"
test_cases['exercice_3.sh'] = {
      'tests': {
          'bill': {'input': input_3_1, 'output': answer_3(input_3_1), 'show': 1},
          'francis': {'input': input_3_2, 'output': answer_3(input_3_2)},
          'louis': {'input': input_3_3, 'output': answer_3(input_3_3)},
          'zoey': {'input': input_3_4, 'output': answer_3(input_3_4)}
      }
}
#+END_SRC

#+NAME: solution_3
#+BEGIN_SRC shell :tangle Shell_3/cf/exercice_3.sh
#!/bin/bash
num=1
while read line
do
      echo $line | sed 's/  */ /g' | cut -d' ' -f1-$num
      num=$(expr $num + 1)
done
#+END_SRC

#+BEGIN_SRC shell :tangle Shell_3/wrong/exercice_3.sh
#!/bin/bash
num=0
while read line
do
      echo $line | sed 's/  */ /g' | cut -d' ' -f1-$num
      num=$(expr $num + 1)
done
#+END_SRC

** Exercice 4

Ecrivez un script qui, pour chaque argument $x$ qui lui est donné, exécute le script de la question
3 en lui donnant pour entrée le contenu du fichier $x$. Le script demandé, =exercice_4.sh=, devra
produire pour tout affichage l'ensemble des lignes produites par toutes ces exécutions, trié à l'aide
de la commande =sort= (on trie les lignes par ordre alphabétique de leur contenu, pas d'histoire de
mots ici). Remarque : si vous n'avez pas réussi l'exercice 3, pas d'inquiétude, le
script d'évaluation placera automatiquement dans =exercice_3.sh= une version correcte de ce script.

#+BEGIN_SRC python :tangle Shell_3/ef/vpl_evaluate.cases :output append :noweb yes
import locale

def output_4(*args):
    lines = []
    for input in args:
        lines = lines + lines_3(input)
    return "\n".join(sorted(lines))


setup_4=""
file_content={
    'a': input_3_1,
    'b': input_3_2,
    'c': input_3_3,
    'd': input_3_4
}
for name in file_content:
    setup_4 = setup_4 + "echo '{}' >{}.txt; ".format(file_content[name], name)
setup_4 = setup_4 + "cat >exercice_3.sh <<'END'{}END\nchmod u+x exercice_3.sh".format(r"""
    <<solution_3>>
""")

test_cases['exercice_4.sh'] = {
      'tests': {
          'final': {'setup': setup_4,
                     'args': ["a.txt", "b.txt", "c.txt", "d.txt"],
                     'output': output_4(input_3_1, input_3_2, input_3_3, input_3_4)}
      }
}
#+END_SRC

#+BEGIN_SRC shell :tangle Shell_3/cf/exercice_4.sh
#!/bin/bash
for file in $*
do
    ./exercice_3.sh <$file >>tmp
done
sort tmp
#+END_SRC

#+BEGIN_SRC shell :tangle Shell_3/wrong/exercice_4.sh
#!/bin/bash
for file in $*
do
    ./exercice_3.sh <$file
done
#+END_SRC
