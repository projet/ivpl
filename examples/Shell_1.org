#+OPTIONS: num:nil
#+OPTIONS: toc:nil
#+OPTIONS: html-postamble:nil
#+OPTIONS: html-preamble:nil
#+LANGUAGE: fr
#+EXPORT_EXCLUDE_TAGS: noexport
#+PROPERTY: header-args :eval never :mkdirp yes :exports none

Dans cette partie, pour chaque question, il vous est demandé de
fournir un fichier de commandes (ou script) répondant au
problème. Votre script pourra commencer par la ligne =#!/bin/bash=
mais il fonctionnera aussi sans.

Vous pouvez tester votre solution grâce au bouton ='Run'= qui ouvre un
terminal dans lequel vous pouvez :
- exécuter votre solution grâce à la commande =./exercice_X.sh= où =X= est le numéro de l'exercice
- examiner le répertoire courant pour vérifier que votre solution a bien fait ce qu'il fallait
Le bouton ='Debug'= fait la même chose que ='Run'= mais affiche en plus toutes les commandes exécutée
(précédées d'un caractère ='+'=).

*ATTENTION* : rappelez vous que le bouton ='Evaluate'= évalue tous les exercices pour vous donner une
note. Ne le pressez que lorsque vous avez répondu à toutes les questions.

** Files initialization :noexport:

#+BEGIN_SRC shell :tangle Shell_1/remote_settings.sh
vplid=49765
#+END_SRC

#+BEGIN_SRC python :tangle Shell_1/ef/vpl_evaluate.cases
# This file is plain Python so it might contain pieces of code that will be evaluated
# before the execution of any test

test_cases = {
      'execute': 'bash -O globasciiranges -- $NAME "$@"'
}
#+END_SRC

#+BEGIN_SRC shell :tangle Shell_1/ef/vpl_custom_setup.sh
# This is a setup that users will see when using 'Run' or 'Evaluate'
# It is run once for all the testsuite
echo $RANDOM > exercice_1.txt
echo $RANDOM > secret.txt
mkdir Exercice_2
cd Exercice_2
touch fichier$RANDOM
touch fichier${RANDOM}bis
dir=Essai$RANDOM
mkdir $dir
touch $dir/fichier$RANDOM
cd ..
#+END_SRC

** Exercice 1

Dans le répertoire courant :
- créez un répertoire nommé =ProgShell=
- copiez le fichier =exercice_1.txt= dans le répertoire =ProgShell=
- créez un fichier vide nommé =mon_fichier.txt= dans le répertoire =ProgShell=

#+BEGIN_SRC shell :tangle Shell_1/cf/exercice_1.sh
mkdir ProgShell
cp exercice_1.txt ProgShell
touch ProgShell/mon_fichier.txt
#+END_SRC

#+BEGIN_SRC shell :tangle Shell_1/wrong/exercice_1.sh
mkdir ProgShell
cp exercice_1.txt ProgShell.txt
touch ProgShell/mon_fichier.txt
#+END_SRC

#+BEGIN_SRC python :tangle Shell_1/ef/vpl_evaluate.cases
# We find out the randomly generated part from global setup
with open("exercice_1.txt") as f:
    content = f.read()
content = content.rstrip()

# We define a small shell script that checks the first exercise
validator1 = "content='{}'".format(content) + r"""
        [ "$(cat exercice_1.txt)" = "$content" ] && \
        diff exercice_1.txt ProgShell/exercice_1.txt && \
        [ -f ProgShell/mon_fichier.txt ] && \
        [ -z "$(cat ProgShell/mon_fichier.txt)" ]
"""

# Each testcase is named, testcases are executed in alphabetical order without cleanup
test_cases['exercice_1.sh'] = {
          # Tests to run for this testcase, no cleanup between tests
          'tests': {
              # Each test is named
              # A validator is a shell sequence whose last command is the test result
              'simple': {'validator': validator1}
          }
      }
#+END_SRC

** Exercice 2

Dans le répertoire courant :
- deplacez le fichier =secret.txt= dans le répertoire =Exercice_2=
- déplacez vous dans le répertoire =Exercice_2=
- affichez le répertoire courant
- affichez le contenu du fichier =secret.txt=
- effacez tous les fichiers et répertoires du répertoire courant

#+BEGIN_SRC shell :tangle Shell_1/cf/exercice_2.sh
mv secret.txt Exercice_2
cd Exercice_2
pwd
cat secret.txt
rm -r *
#+END_SRC

#+BEGIN_SRC shell :tangle Shell_1/wrong/exercice_2.sh
mv secret.txt Exercice_2
cd Exercice_2
# pwd
cat secret.txt
rm -r *
#+END_SRC

#+BEGIN_SRC python :tangle Shell_1/ef/vpl_evaluate.cases
with open("secret.txt") as f:
    content2 = f.read().rstrip()

test_cases['exercice_2.sh'] = {
          # By default the output is cleaned up from its extraneous separators, this
          # can be disapled for the testcase or for each test individually
          'cleanup_level': 0,
          'tests': {
              # A validator can also be given directly when defining the entry (any valid Python construct is ok)
              # Note that the validator has access to the output produced by the student program
              # through the file .output.txt
              'simple': {
                  'validator': "content='{}'".format(content2) +
                  r"""
                      cd Exercice_2
                      pwd >../verif.txt
                      echo $content >>../verif.txt
                      [ ! -f ../secret.txt ] && [ -z "$(ls)" ] && diff ../.output.txt ../verif.txt
                  """
              }
          }
      }
#+END_SRC

** Exercice 3

*ATTENTION* : À partir de cet exercice, les fichiers sur lesquels votre solution sera testée
n'apparaissent plus lorsque vous utilisez ='Run'= ou ='Debug'=, c'est à vous d'imaginer toutes les
situations pertinentes et de tester la correction de votre script.

Dans le répertoire courant (ne contenant aucun sous répertoire) :
- affichez tous les fichiers dont le nom contient exactement 3 caractères, commence par une lettre
  minuscule et termine par un chiffre
- affichez tous les fichiers dont le nom contient au moins une lettre majuscule

#+BEGIN_SRC shell :tangle Shell_1/cf/exercice_3.sh
ls [a-z]?[0-9]
ls *[A-Z]*
#+END_SRC

#+BEGIN_SRC shell :tangle Shell_1/wrong/exercice_3.sh
ls [a-z]?[0-9]
ls *[A-Z]
#+END_SRC

#+BEGIN_SRC python :tangle Shell_1/ef/vpl_evaluate.cases
test_cases['exercice_3.sh'] = {
          # A test case can have a setup not visible by students, completely executed before each test
          # that can be overriden in each test
          'setup': 'rm -r Exercice_2 ProgShell; touch lM2 Abracadabra tototiti Ab4 jkl17 42a aba 421',
          'cleanup_level': 3,
          'tests': {
              # Rather than a validator, the output that should be produced can be given
              'simple': {'output': "lM2\nAb4\nAbracadabra\nlM2"}
          }
      }
#+END_SRC

** Exercice 4

Dans le répertoire courant :
- enlevez le droit en écriture pour le groupe et les autres à tous les fichiers donnés en arguments
- donnez vous le droit d'exécuter le fichier dont le nom est donné en premier argument
- exécutez le fichier dont le nom est donné en premier argument en lui donnant comme arguments tous
  les arguments passés à votre script

#+BEGIN_SRC shell :tangle Shell_1/cf/exercice_4.sh
chmod go-w $*
chmod +x $1
./$1 $*
#+END_SRC

#+BEGIN_SRC shell :tangle Shell_1/wrong/exercice_4.sh
chmod go+w $*
chmod +x $1
./$1 $*
#+END_SRC

#+BEGIN_SRC python :tangle Shell_1/ef/vpl_evaluate.cases
# We defined a function that returns a shell script that checks the fourth exercise
def generate_validator(name):
      return r"""[ -z "$(tail -n +2 .arguments.txt | xargs ls -l | cut -c6,9 | grep w)" ] && \
                 [ -n "$(ls -l {} | cut -c6,9 | grep w)" ]""".format(name)

test_cases['exercice_4.sh'] = {
          'setup': 'echo echo \$3 \$2 > prog1.sh;touch prog2.sh prog3.sh tata.txt;' +
                   'chmod o+w *.sh; chmod g+w tata.txt',
          'tests': {
              # A test run can be given command line arguments
              # Note that the validator has access to them (file .arguments.txt, see in generate_validator)
              'first': {'args': ["prog1.sh", "prog3.sh", "tata.txt"],
                        'output': 'tata.txt prog3.sh',
                        'validator': generate_validator("prog2.sh")
              },
              # When both an expected output and a validator are given, both should be satisfied
              # for the test to pass
              'second': {'args': ["prog1.sh", "prog2.sh", "prog3.sh"],
                         'output': 'prog3.sh prog2.sh',
                         'validator': generate_validator("tata.txt")
              }
          }
      }
#+END_SRC
