#include <stdio.h>
#include "fonction.h"

int main() {
    int a, b;

    scanf("%d %d", &a, &b);
    fonction(&a, &b);
    printf("%d %d\n", a, b);
    return 0;
}
