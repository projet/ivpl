#include <stdio.h>
#include "compter_les_lettres.h"

int main() {
    int decompte[26] = {0};

    compter(stdin, decompte);

    for (int i=0; i<26; i++)
        if (decompte[i] > 0)
            printf("%c: %d\n", 'a'+i, decompte[i]);
    return 0;
}

