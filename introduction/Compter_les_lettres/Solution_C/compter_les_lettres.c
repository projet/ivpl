#include <stdio.h>

void compter(FILE *f, int *decompte) {
    char c;

    fscanf(f, "%c", &c);
    while (!feof(f)) {
        if ((c >= 'a') && (c <= 'z'))
            decompte[c-'a']++;
        fscanf(f, "%c", &c);
    }
}
