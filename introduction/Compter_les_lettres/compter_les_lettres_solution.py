#!/usr/bin/env python3
import sys

decompte = {}
for l in range(26):
    decompte[chr(ord('a')+l)] = 0
f = sys.stdin
c = f.read(1)
while len(c) > 0:
    if (c >= 'a') and (c <= 'z'):
        decompte[c] += 1
    c = f.read(1)

for key in decompte:
    if decompte[key]:
        print(key + ": " + str(decompte[key]))
