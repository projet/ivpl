#!/usr/bin/env python3
import sys

lettres = []
decompte = []
f = sys.stdin
c = "init"
while len(c) > 0:
	c = f.read(1)
	if (c >= 'a') and (c <= 'z'):
		found = False
		for i in range(len(lettres)):
			if c == lettres[i]:
				decompte[i] += 1
				found = True
		if not found:
			lettres.append(c)
			decompte.append(1)

for i in range(len(lettres)):
	print(lettres[i] + ": " + str(decompte[i]))
