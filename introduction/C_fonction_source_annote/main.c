#include <stdio.h>

void fonction(int *a, int *b) {
#ifdef SOLUTION
  int tmp = *a;
  *a = *b;
  *b = tmp;
#else /* SOLUTION */
  // à compléter, échanger les valeurs de a et b
#endif /* SOLUTION */
}

int main() {
  int a, b;

  printf("Veuillez saisir deux entiers :\n");
  scanf("%d %d", &a, &b);
  fonction(&a, &b);
  printf("Après l'appel à fonction(&a, &b), a vaut %d et b vaut %d\n", a, b);

  return 0;
}
