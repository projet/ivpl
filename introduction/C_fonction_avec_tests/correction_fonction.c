#include "fonction.h"

void fonction(int *a, int *b) {
  int tmp = *a;
  *a = *b;
  *b = tmp;
}
