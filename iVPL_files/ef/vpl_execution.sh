#!/usr/bin/env bash
# Files to keep until the very last moment
PRECIOUS_FILES="vpl_custom_setup.sh vpl_custom_setup.d"

error_message() {
    message=$1
    text=$2
    if [ "$MODE" != evaluate ]
    then
        echo -e "\033[31m*** $message ***\033[0m $text"
    else
        echo "<|--"
        echo "-$message"
        echo "$text"
        echo "--|>"
    fi
    exit 1
}

internal_error() {
    error_message "Internal Error" "$1"
}

error() {
    error_message "Error" "$1"
}

remove_any() {
    if [ -f $1 ]
    then
        rm $1
    fi
}

search() {
    needle=$1
    shift
    for hay in $*
    do
        if [ "$needle" = "$hay" ]
        then
            return 0
        fi
    done
    return 1
}

# Default settings
if [ -z "$1" ]
then
    internal_error "mode is missing for vpl_execution.sh"
fi
MODE=$1
shift

# Locales are a source of many bad behavior (glob in bash, sort order, ...)
# Changing locale has a handful of negative impacts (vim behavior,
# compiling code with accents), so we don't change it
# export LC_ALL=C
# vim doesn't work on the server with C as LC_ALL, here's the fix
# echo 'set encoding=utf-8' >>.vimrc
# We rely instead on globasciiranges
shopt -s globasciiranges 2>/dev/null || true
# Note that globasciiranges will be enbaled by default in bash 5.0, this solves half of the issue

# Can cause unwanted extra ouputs if set
unset CDPATH

# If mode is not evaluate, removes any file which is not submitted or
# explicitely precious. We execute this before the custom setup so that
# any file created by the custom setup will not be removed
if [ "$MODE" != evaluate ]
then
    for file in $(find . -type f | cut -d/ -f2- | grep -v '^\.')
    do
        if ! search $file $VPL_SUBFILES $PRECIOUS_FILES
        then
            dir=$(dirname $file)
            rm -fr $file
            rmdir -p $dir 2>/dev/null
        fi
    done
    # adjust the terminal size to the actual window
    resize >/dev/null
fi

# Runs custom extensions
# (typically to compile things or decide on the executable)
if [ -d vpl_custom_setup.d ]
then
    for file in vpl_custom_setup.d/*
    do
        source $file
        echo "Completed custom setup for $file"
        rm $file
    done
    rmdir vpl_custom_setup.d
fi

# Runs custom setup (common to all modes)
if [ -f vpl_custom_setup.sh ]
then
    source ./vpl_custom_setup.sh
    echo "Completed custom setup"
    rm vpl_custom_setup.sh
fi

# If mode is not evaluate
if [ "$MODE" != evaluate ]
then
    # Runs appropriately depending on the mode and some variables
    case $MODE in
        run)
            if [ -n "$RUN" ]
            then
                exec bash -c "$RUN" -- $ARGUMENTS
            else
                exec bash
            fi
            ;;
        debug)
            if [ -n "$DEBUG" ]
            then
                exec bash -c "$DEBUG" -- $ARGUMENTS
            else
                echo "set -x" >.bashrc
                exec bash
            fi
            ;;
        *)
            internal_error  "Invalid mode"
            ;;
    esac
else
    if [ ! -f vpl_evaluate.py ]
    then
        internal_error "vpl_evaluate.py missing"
    fi
    chmod +x vpl_evaluate.py || internal_error "can't make vpl_evaluate.py executable"
    exec ./vpl_evaluate.py $* $EVALUATE_MODE || internal_error "can't exec vpl_evaluate.py"
fi
