#!/usr/bin/env python3
import os


class Match:
	def __init__(self, arg):
		self.parameters = arg

	def examine_element(self, expression, data):
		if issubclass(type(expression), Match):
			return expression.examine(data)
		else:
			if expression == data:
				return None
			else:
				return ""

	def examine(self, data):
		return self.examine_element(self.parameters, data)

	def apply_to_element(self, expression, method, *args):
		if issubclass(type(expression), Match):
			return expression.apply(method, *args)
		else:
			return method(expression, *args)

	def apply(self, method, *args):
		self.parameters = self.apply_to_element(self.parameters, method, *args)
		return self

	def __str__(self):
		return self.parameters.__str__()

	def __repr__(self):
		return self.parameters.__repr__()


class MatchMultiple(Match):
	def __init__(self, *args):
		self.parameters = list(args)

	def apply(self, method, *args):
		for i in range(len(self.parameters)):
			self.parameters[i] = self.apply_to_element(self.parameters[i], method, *args)
		return self

	def optional_str(self):
		return "";

	def __str__(self):
		return type(self).__name__+"("+",".join([expression.__str__() for expression in self.parameters])+self.optional_str()+")"

	def __repr__(self):
		return type(self).__name__+"("+",".join([expression.__repr__() for expression in self.parameters])+self.optional_str()+")"


class MatchWord(MatchMultiple):
	import re

	def get_candidates_list(self, data):
		return self.re.findall(r"\w+", data)

	def validate(self, a, b):
		return a.lower() == b.lower()

	def __init__(self, *args, strict=False):
		self.strict = strict
		MatchMultiple.__init__(self, *args)

	def examine(self, data):
		candidates = self.get_candidates_list(data)
		if (len(candidates) < len(self.parameters)) or (self.strict and (len(candidates) > len(self.parameters))):
			start = f"{candidates} requires "
			if (len(candidates) < len(self.parameters)):
				start += "more"
			else:
				start += "less"
			return f"{start} elements in order to match {self.parameters}"
		for i in range(-1, -(len(self.parameters)+1),-1):
			if not self.validate(candidates[i], self.parameters[i]):
				return f"{candidates[i]} in {candidates[-len(self.parameters):]} should have matched {self.parameters[i]} in {self.parameters}"
		return None

	def optional_str(self):
		if self.strict:
			return ",strict=True";
		else:
			return "";


class MatchNumber(MatchWord):
	EPSILON=1E-5;

	def square(self, number):
		return number*number

	def get_candidates_list(self, data):
		return self.re.findall(r"-?\d*(?:\d\.|\.\d|\d)\d*(?:[Ee]-?\d+)?", data)

	def validate(self, a, b):
		return self.square(float(a) - float(b)) <= self.precision

	def __init__(self, *args, strict=False, precision=EPSILON):
		self.precision = self.square(precision)
		MatchWord.__init__(self, *args, strict=strict)


class MatchAny(MatchMultiple):
	def convert(self, result):
		if result is None:
			return True
		else:
			return False

	def start(self):
		return False

	def combine(self, state, value):
		return state or value

	def done(self, state):
		return state

	def negative_result(self, data, failed_list):
		return "it should have matched any of the possibilities in the Match:\n- "+"\n- ".join([Match(s).__repr__() for s in failed_list])

	def examine(self, data):
		state = self.start()
		for i in range(len(self.parameters)):
			new_value = self.examine_element(self.parameters[i], data)
			state = self.combine(state, self.convert(new_value))
			if self.done(state):
				if state:
					return None
				else:
					return self.negative_result(data, self.parameters[0:i+1])
		if state:
			return None
		else:
			return self.negative_result(data, self.parameters)


class MatchAll(MatchAny):
	def start(self):
		return True

	def combine(self, state, value):
		return state and value

	def done(self, state):
		return not state

	def negative_result(self, data, failed_list):
		return f"it should, at least, have matched: {Match(failed_list[-1]).__repr__()}"


class MatchRegex(MatchAll):
	import re

	def __init__(self, *args, flags=0):
		self.flags = flags
		MatchAll.__init__(self, *args)

	def examine_element(self, expression, data):
		if self.re.search(expression, data, flags=self.flags):
			return None
		else:
			return ""

	def optional_str(self):
		if self.flags:
			return f", flags={self.flags}"
		else:
			return ""

class MatchNone(MatchAny):
	def start(self):
		return True

	def combine(self, state, value):
		return state and not value

	def done(self, state):
		return not state

	def negative_result(self, data, failed_list):
		return f"it should, at least, not have matched: {Match(failed_list[-1]).__repr__()}"


class IVPL:
	import errno
	import fcntl
	import select
	import shutil
	import signal
	import subprocess
	from subprocess import PIPE
	import sys
	import re
	import string
	import tempfile
	import traceback
	import pprint

	version = 0.3
	pprinter = pprint.PrettyPrinter()
	colors = {
		"BLUE": "\033[34m",
		"GREEN": "\033[32m",
		"RED": "\033[31m",
		"YELLOW": "\033[33m",
		"BB": "\033[40m",
		"NC": "\033[0m"
	}

	def __init__(self):
		# We ignore sigpipes for the execution part that collects stdout and stdin
		self.signal.signal(self.signal.SIGPIPE, self.signal.SIG_IGN)
		for arg in self.sys.argv[1:]:
			if arg.startswith("no"):
				arg = arg[2:]
				mode[arg] = False
			else:
				mode[arg] = True
		self.files = {}
		self.executables_list = []


	def balised_output(self, balised, prefix, suffix, *args, **kwargs):
		end = kwargs.get("end")
		kwargs["end"] = ''
		if balised:
			print(prefix, **kwargs)
		print(*args, **kwargs)
		if balised:
			print(suffix, **kwargs)
		if end is None:
			del kwargs["end"]
		else:
			kwargs["end"] = end
		print("", **kwargs)


	def colored_output(self, color, *args, **kwargs):
		global mode
		self.balised_output(mode.get("local"), self.colors[color], self.colors["NC"], *args, **kwargs)


	def comment(self, *args, **kwargs):
		global mode
		self.balised_output(not mode.get("local"), "<|--\n", "\n--|>\n", *args, **kwargs)


	def title(self, msg):
		global mode
		if mode.get("local"):
			self.colored_output('BLUE', msg)
		else:
			self.comment("-" + msg)


	def grade(self, value):
		self.hidden("Grade :=>>{:.2f}".format(value))


	def error(self, *args, **kwargs):
		global mode
		if mode.get("local"):
			self.colored_output('RED', "Internal error : ", end='')
		else:
			self.title("Internal error")
		self.comment(*args, **kwargs)
		exit(1)


	def warning(self, *args, **kwargs):
		self.colored_output('RED', *args, **kwargs)


	def debug(self, *args, **kwargs):
		global mode
		if mode.get("debug"):
			self.colored_output('GREEN', *args, **kwargs)


	def hidden(self, *args, **kwargs):
		self.colored_output('YELLOW', *args, **kwargs)


	def remove(self, *args):
		global mode
		if not mode.get("keep_files"):
			for name in args:
				try:
					if os.path.exists(name):
						os.unlink(name)
				except Exception as e:
					self.error("Unlink of file resulted in " + str(e))
					return 0
		return 1


	def add_executable(self, *args):
		for name in args:
			self.executables_list.append(name)


	def run_command(self, test_name, command, test, has_limits):
		if 'args' not in test:
			arguments = []
		else:
			arguments = list(map(str, test['args']))

		actual_command = 'exec ' + command + ' "$@"'
		timeout = 0
		if has_limits:
			timeout = test.get('timeout', 10)
			if timeout > 0:
				arguments = [command] + arguments
				actual_command = 'exec timeout -s9 --preserve-status {} "$@"'.format(timeout)

		# We use the low level interface in order to stop collecting outputs when the limit is reached
		data = {}
		try:
			# The shell is probably impossible to avoid if we want to let a mixture of binaries,
			# well formed scripts and bare text to be accepted as executable
			# The alternative of enforcing shebang is probably a bad idea as the user might provide
			# the execute field with any interpreter launch command
			arguments = [actual_command, '--'] + arguments
			process = self.subprocess.Popen(arguments, stdin=self.PIPE, stdout=self.PIPE, stderr=self.PIPE, shell=True)
			part2stream = {
				'input': process.stdin,
				'output': process.stdout,
				'error': process.stderr
			}
			remaining = {}
			done = {}
			part2num = {}
			num2part = {}
			for part in ('input', 'output', 'error'):
				part2num[part] = part2stream[part].fileno()
				num = part2num[part]
				num2part[num] = part
				# Our I/Os are non blocking
				self.fcntl.fcntl(num, self.fcntl.F_SETFL, os.O_NONBLOCK)
				if has_limits:
					remaining[part] = test.get(part + '_limit', 10 * 1024)
				else:
					remaining[part] = -1
				done[part] = 0
				data[part] = ''
			data['input'] = str(test.get('input', ''))
			remaining['input'] = len(data['input'])
			# Debug information about what's happening
			msg = test_name + ": executing " + command
			separator = " with "
			if has_limits:
				for part in ('output', 'error'):
					msg += separator + part
					if remaining[part] >= 0:
						msg += " limited to {} bytes".format(remaining[part])
					else:
						msg += " unlimited"
					separator = ", "
				msg += " and "
				if timeout > 0:
					msg += "timeout at {}".format(timeout)
				else:
					msg += "no timeout"
			self.debug(msg)

			poller = self.select.poll()
			if remaining['input']:
				poller.register(part2num['input'], self.select.POLLOUT)
				set_size = 3
			else:
				part2stream['input'].close()
				set_size = 2
			poller.register(part2num['output'], self.select.POLLIN)
			poller.register(part2num['error'], self.select.POLLIN)
			while set_size > 0:
				for desc, event in poller.poll():
					part = num2part[desc]
					if remaining.get(part) >= 0:
						size = remaining[part]
					else:
						size = 1024
					try:
						if event == self.select.POLLOUT:
							data_bytes = data[part][done[part]:]
							number = os.write(desc, data_bytes.encode())
						else:
							data_bytes = os.read(desc, size)
							number = len(data_bytes)
							# No handling of the encoding here, it is performed in preformat
							data[part] += data_bytes.decode('ascii', errors='ignore')
						done[part] += number
						if remaining[part] != -1:
							remaining[part] -= number
						if not number or not remaining[part]:
							poller.unregister(desc)
							part2stream[part].close()
							set_size -= 1
						if not remaining[part]:
							data[part + '_bounded'] = True
					except OSError as e:
						if e.errno in [self.errno.EINTR, self.errno.EAGAIN, self.errno.EWOULDBLOCK]:
							self.debug("Access to command {} temporarily failed, {}, size {}, done {}".format(
								part, str(e), size, done[part])
							)
						else:
							self.debug("Error", str(e), "during access to command", part)
			if 'input' in data:
				del data['input']
			if 'input_bounded' in data:
				del data['input_bounded']
			process.wait()
			if process.returncode < 0:
				data['signal'] = -process.returncode
				data['code'] = 0
			else:
				data['signal'] = 0
				data['code'] = process.returncode
			data['failure'] = (process.returncode != 0)
		except OSError as e:
			self.error("Cannot exec", command, "error", str(e))
		return data


	def setup_env(self, test_name, name, program):
		# ARGS is not necessary as it is accessible through $@
		for part in ['files']:
			if 'files' in program:
				os.environ[part.upper()] = " ".join(program['files'].keys())
		os.environ['TESTNAME'] = test_name
		os.environ['NAME'] = name


	def dispatch_error(self, message, data, soft_fail):
		for stream in ('output', 'error'):
			data_value = data.get(stream)
			if data_value is not None and len(data_value) > 0:
				self.warning(data_value)
		if soft_fail:
			if len(message):
				self.warning(message)
		else:
			self.error(message)


	# Runs a named command, described by a given content and associated to a test,
	# returns a hash that holds the content of standard output and error as well as exit status
	# The content might be :
	# - scalar : a script to be executed
	# - a ref to a hash : description of the executable and execution
	# The test holds input and args informations
	def run_special(self, test_name, name, content, test, has_limits):
		special = content
		default_compile = False

		if type(content) is dict:
			# Values of special are written to test to be able to overwrite intput and args in special
			# This is especially useful for a validator
			# Note here that values from the test cannot be inserted in special because, for instance,
			# the test might be compiled while special is not (this might be the case for an exercice to
			# be written in C but whose solution has been written in python
			given_test = test
			test = given_test | special
		else:
			special = {'content': content}

		if 'executable' in special:
			name = special['executable']

		# sets the default compile command before populating files
		# usually, 'content' is the content of the main file so we should compile if :
		# - the name for content does not match the target (filename or extension)
		# - when there are files which do not seem sufficient to provide us with an executable :
		#   - there's no content (otherwise, we can decide based on its name)
		#   - no Makefile included
		#   - the target is not part of the files
		if 'compile' not in special and (
				'filename' in special and special['filename'] != name
				or ('files' in special
					and 'content' not in special
					and 'Makefile' not in special['files']
					and name not in special['files']
				)
				or 'extension' in special
		):
			default_compile = True

		# convert 'files' setting into dict format if needed
		if 'files' in special and type(special['files']) is list:
			new_files = {}
			for filename in special['files']:
				new_files[filename] = None
			special['files'] = new_files

		# generate file if required and populate the 'files' array
		if 'content' in special:
			if 'extension' in special:
				if 'filename' in special:
					self.error(
						"in settings for program {} in test case {}, 'extension' is incompatible with 'filename'".format(name, test_name)
					)
				special['filename'] = name + "." + special['extension']
				del special['extension']
			if 'filename' not in special:
				special['filename'] = name
			if 'files' not in special:
				special['files'] = { special['filename']: special['content'] }
			elif special['filename'] not in special['files'].keys():
				special['files'][special['filename']] = special['content']

		# write required files
		if 'files' in special:
			for filename in special['files'].keys():
				if special['files'][filename] is not None:
					self.ensure_write(filename, special['files'][filename])

		if default_compile:
			special['compile'] = "echo '{}: {}' | /usr/bin/env make -f - {}".format(name, " ".join(special['files'].keys()), name)
		else:
			if ('compile' not in special) and not os.path.exists(name):
				special['compile'] = "/usr/bin/env make {}".format(name)
			else:
				if 'compile' not in special:
					special['compile'] = None

		# compile if required
		if special.get('compile'):
			if 'compiled' in special:
				tmp_name = special['compiled']
				self.debug("Retrieving compiled file", tmp_name, "for", name)
				self.shutil.copy(tmp_name, name)
			else:
				tmpsubdir = os.path.dirname(name)
				tmpsubdirpath = os.path.join(self.tempfile.gettempdir(), tmpsubdir)
				if len(tmpsubdir) and not os.path.exists(tmpsubdirpath):
					os.makedirs(tmpsubdirpath)
				tmp, tmp_name = self.tempfile.mkstemp(dir=self.tempfile.gettempdir(), prefix=name)
				os.close(tmp)
				self.debug("Compiling {}, saving it to {}".format(name, tmp_name))
				self.setup_env(test_name, name, special)
				data = self.run_special(name, '.compile', special['compile'], special, False)
				if data['failure']:
					data['compilation_error'] = True
					return data
				else:
					for part in ('output', 'error'):
						if len(data[part]):
							self.warning("Compilation", part, ":\n", data[part], end='')
					if os.path.exists(name):
						self.shutil.copy(name, tmp_name)
						special['compiled'] = tmp_name
						self.add_executable(tmp_name)
		else:
			try:
				os.chmod(name, 0o755)
			except OSError as e:
				error_msg = "Cannot make {} executable: {}".format(name, str(e))
				if 'compile' not in special:
					error_msg += " (and no 'compile' directive found)"
				self.error(error_msg)

		if 'content' in special and not special.get('compile'):
			self.debug("Executing ", special['content'].splitlines(False)[0], "...")
		result = {}
		if 'execute' in special:
			if special['execute']:
				self.setup_env(test_name, name, special)
				result = self.run_special(test_name, '.execute', special['execute'], test, has_limits)
		else:
			result = self.run_command(test_name, "./{}".format(name), test, has_limits)

		# We delete the file that has been possibly created for the compilation or execution
		if 'files' in special:
			for filename in special['files'].keys():
				if special['files'][filename] is not None:
					self.remove(filename)
		if special.get('compile'):
			self.remove(name)
		return result


	def clean_outputs(self, test_name, data, test):
		if 'cleanup_level' in test:
			level = test['cleanup_level']
		else:
			level = 2
		for part in ('output', 'error'):
			postprocess = None
			if 'postprocessing' in test:
				postprocess = test['postprocessing']
			if part+'_postprocessing' in test:
				postprocess = test[part+'_postprocessing']
			if postprocess is not None:
				new_data = self.run_special(test_name, '.' + part + '_postprocessing', postprocess, {'input': data[part]}, False)
				if len(new_data['error']):
					self.warning("Postprocessing produced data on stderr for {}:".format(part))
					self.warning(new_data['error'])
				if new_data['failure']:
					self.warning("*** POSSIBLE ERROR : Postprocessing of {} exited with ".format(part) + self.format_error(new_data))
				data[part] = new_data['output']
			if len(data[part]) and self.re.match(r'Grade\s*:=>>', data[part], self.re.IGNORECASE):
				data[part] = "Pathetic attempt to set the grade detected\n"
				data[part] += "Please stop trying to hack the system"
			data[part] = self.cleanup(data[part], level)
			if part in test:
				test[part] = Match(test[part])
				test[part].apply(self.cleanup, level)


	def cleanup(self, data, level):
		if data:
			data = str(data)
			if level > 0:
				# Removes trailing newlines
				data = self.re.sub(r'\n*$', '', data)
				if level > 0.5:
					# Removes blank lines at the beginning and the end, as well as trailing blanks
					data = self.re.sub(r'^\s*\n', '', data)
					data = self.re.sub(r'\n\s*$', '', data)
					data = self.re.sub(r'[ \t]+$', '', data, flags=self.re.MULTILINE)
					if level > 1:
						# Removes leading blanks
						data = self.re.sub(r'^[ \t]+', '', data, flags=self.re.MULTILINE)
						if level > 1.5:
							# Replaces tabs with spaces # and sequences of spaces with a single space
							data = self.re.sub(r'\t', ' ', data)
							data = self.re.sub(r' +', ' ', data)
							if level > 2:
								# Removes blank lines
								data = self.re.sub(r'\n+\n', '\n', data)
								if level > 2.5:
									# Join lines with spaces
									data = self.re.sub(r'\n', ' ', data)
		return data


	def preformat(self, data):
		data = str(data)
		# debug stuff, our string should be ascii
		if mode.get('debug'):
			try:
				line = data.encode('ascii')
			except:
				raise(Exception("Non ascii line : " + data))
		line = self.re.sub(r'^(.)', r'>\1', data, flags=self.re.DOTALL | self.re.MULTILINE)
		desirable_characters = self.string.digits+self.string.ascii_letters+self.string.punctuation+' \t\n'
		line = self.re.sub(r'[^' + self.re.escape(desirable_characters) + r']', '.', line)
		if len(line) == 0 or line[-1] != '\n':
			line += '\n'
		return line


	def format_error(self, data):
		result = ""
		if data['code']:
			result += "code " + str(data['code'])
		if data['signal']:
			if len(result):
				result += " and "
			result += "signal " + str(data['signal'])
		return result


	def run_program(self, test_name, name, test, context, has_limits, directory):
		if directory is not None:
			original = os.getcwd()
			self.debug(f"Going to directory {directory} for the execution")
			os.chdir(directory)
		if 'setup' in context:
			setup = self.run_special(test_name, '.setup', context['setup'], context, False)
			if len(setup['output']) or len(setup['error']):
				self.warning("Setup output and error:")
			if setup['failure']:
				msg = "*** POSSIBLE ERROR : Setup exited with " + self.format_error(setup)
			else:
				msg = ""
			self.dispatch_error(msg, setup, 1)
		result = self.run_special(test_name, name, test, context, has_limits)
		if directory is not None:
			os.chdir(original)
		return result


	# runs all tests for a given testcase, returns the score (percentage)
	# TODO : a cleanup / restore mechanism for local files ? (using tar for instance)
	def perform_tests(self, program, case, directories):
		global mode
		tests = case["tests"]
		score = 0
		total = 0
		points_reduction = 0
		percent_reduction = 0

		if type(tests) is not dict:
			self.error("'tests' is not a dict for testcase " + program)
		if case.get('sorted'):
			keys = sorted(tests.keys())
		else:
			keys = tests.keys()
		for test_name in keys:
			if type(tests[test_name]) is not dict:
				self.error("Test " + test_name + " for testcase " + program + " is not a dict")
			break_loop = False
			test = case | tests[test_name]

			weight = test.get('weight', 1)
			total += weight
			indication = test_name
			if 'weight' in test:
				indication += " (weight {})".format(weight)
			result = None
			details = "Test {}:\n".format(test_name)

			# runs program
			if 'submission' in test:
				submission_description = test['submission']
			else:
				submission_description = test
			submission = self.run_program(test_name, program, submission_description, test, True, directories.get('program'))

			if submission.get('compilation_error'):
				details += "Compilation errors, compiler output :\n"
				details += self.preformat(submission['output']) + self.preformat(submission['error'])
				result = 0
				break_loop = True
			else:
				if test.get('compiled'):
					case['compiled'] = test['compiled']
				if 'input' in test:
					details += "With the input:\n" + self.preformat(test['input'])
				if 'args' in test:
					test['args'] = list(map(str, test['args']))
					details += "With the arguments: [ {} ]\n".format(", ".join(test['args']))
				# We clean outputs of both program output and expected output
				self.clean_outputs(test_name, submission, test)

				# runs a provided solution for the testcase
				# this will populate test->{output,error,code} for the following test
				if 'solution' in test:
					solution = self.run_program(test_name, '.solution', test['solution'], test, False, directories.get('solution'))
					# If the solution does not compile, we're in trouble
					if solution.get('compilation_error'):
						self.error("Solution failed to compile, stderr is\n"+solution['error'])
					# We clean outputs of both program output and expected output
					self.clean_outputs(test_name, solution, test)
					# We do not test if the run fails because this might be expected by the exercise
					if 'match' in test:
						match = test['match']
					else:
						match = ['output']
					for part in ('output', 'error', 'code', 'signal'):
						if part in match:
							test[part] = solution[part]
						else:
							if part == 'code' or part == 'signal':
								should_display = solution[part] > 0
							else:
								should_display = len(solution[part]) > 0
							if should_display:
								self.debug("Solution {} :\n{}".format(part, self.preformat(solution[part])), end='')

				# Stores results in files for both the validator and the upcoming display
				for part in ('output', 'error', 'code', 'signal'):
					self.ensure_write(".{}.txt".format(part), submission.get(part))
					if part in test:
						self.ensure_write(".expected_{}.txt".format(part), test[part])
				# runs a validator
				# TODO : in the current state, probably DOES NOT WORK with separate directory, investigation to be done
				if 'validator' in test:
					self.ensure_write(".program_name.txt", program)
					self.ensure_write(".arguments.txt", "\n".join(test.get('args') or []))
					if len(directories):
						for name in directories.keys():
							os.environ[name.upper() + "_DIR"] = directories[name]
					validator = self.run_special(test_name, '.validator', test['validator'], test, False)
					# For potential security issues, we remove the env variable indicating execution directories
					if len(directories):
						for name in directories.keys():
							del os.environ[name.upper() + "_DIR"]
					result = not validator['failure']
					for part in ('output', 'error'):
						if len(validator[part]) > 0:
							details += "Validator {} is:\n".format(part) + self.preformat(validator[part])
					if not result:
						details += "The validator did not validate the test ({})\n".format(self.format_error(validator))
					self.debug("Done with validator subtest with result", result)
					self.remove(".program_name.txt", ".arguments.txt")

				# compare produced output to expected one for each category
				description = {
					'output': "produced on standard output",
					'error': "produced on standard error",
					'code': "returned the code",
					'signal': "received the signal"
				}
				for part in ('output', 'error', 'code', 'signal'):
					if part in test or submission[part]:
						details += "Your program {}".format(description[part])
						if part in test:
							# test for submission overflow
							if submission.get(part + "_bounded"):
								details += ":\n"+self.preformat(submission[part])
								details += "Your program standard $part has reached the upper limit (failure)\n"
								result = False
							else:
								# match submission with expected output
								explanation = Match(test[part]).examine(submission[part])
								local_result = explanation is None
								if not local_result:
									if explanation:
										details += ":\n" + self.preformat(submission[part])
										details += f"Unfortunately, to match {test[part].__repr__()}:\n{self.preformat(explanation)}"
									else:
										diff = { 'code': 127 }
										if test.get('side_by_side'):
											wopt=""
											if type(test.get('side_by_side')) == str:
												wopt=test.get('side_by_side')
											diff = self.run_special(test_name, ".diff", f"diff -y {wopt} .{part}.txt .expected_{part}.txt", {}, False)
										if diff.get('code') == 127:
											details += ":\n"+self.preformat(submission[part])
											details += "But the expectation was:\n" + self.preformat(test[part])
										else:
											details += " (got / expected):\n" + self.preformat(str(diff['output']))
								else:
									details += ":\n"+self.preformat(submission[part])
									details += "(as expected)\n"
								if result is None:
									result = True
								result = result and local_result
								self.debug("Done with", part, "subtest with result", local_result)
						else:
							details += ":\n" + self.preformat(submission[part])
						if not result and (part == 'signal'):
							if submission['signal'] == 9:
								details += "Looks like a timeout, did you write an infinite loop ?\n"
							elif submission['signal'] == 13:
								details += "Looks like your program produces to much output on stdout/stderr."
								details += " This might be caused by an infinite loop.\n"
					# remove any temporary file created to store the result
					self.remove(".{}.txt".format(part), ".expected_{}.txt".format(part))
				if result is None:
					self.error("No method defined to check", test_name, "result !")
					result = False

			# collect result
			if result:
				self.debug(details, end='')
				self.comment("* Test", indication, "PASSED !")
				score += weight
			else:
				if not test.get('show'):
					self.hidden(details, end='')
					details = "(hidden)"
				if "fail_message" in test:
					try:
						fail_message = eval(test["fail_message"])
					except:
						fail_message = test["fail_message"].strip('\n')
					details = fail_message
				self.comment("*", details, indication, "FAILED !")
				if 'grade_reduction' in test:
					if '%' in test['grade_reduction']:
						percent_reduction += float(test['grade_reduction'].strip('%'))
					else:
						points_reduction += float(test['grade_reduction'])
					score += weight
			if break_loop:
				break
		return (score / total, points_reduction, percent_reduction)


	def read_file(self, name):
		try:
			with open(name, 'r') as f:
				result = f.read()
			return result
		except OSError:
			return None


	def read_and_remove(self, name):
		global ivpl_files
		if name not in self.files:
			self.files[name] = self.read_file(name)
			self.remove(name)
		resultat = self.files[name]
		if resultat is None:
			self.error("Cannot read {} !".format(name))
		return resultat


	def ensure_write(self, name, data):
		try:
			with open(name, 'w') as f:
				if data is not None:
					f.write(str(data))
		except OSError:
			self.error("Cannot write {}".format(name))


	def get_files(self, *unused, required=[], solution=[]):
		result = {}
		for file in required:
			result[file] = None
		for file in solution:
			result[file] = self.read_and_remove(file)
		return result


	def evaluate_file(self, name):
		if os.path.exists(name):
			description = self.read_file(name) or self.error("Cannot read " + name)
			try:
				exec(description, globals())
				self.remove(name)
			except SyntaxError as e:
				self.debug(f"Error: {e}")
				self.error("Evaluation of " + name + " resulted in a syntax error at line " + str(e.lineno))
			except Exception as e:
				exc_type, exc_value, exc_traceback = self.sys.exc_info()
				tb = self.traceback.extract_tb(exc_traceback)
				fs = tb.pop()
				while fs.filename != "<string>" and len(tb) > 0:
					fs = tb.pop()
				self.debug(f"Stack frame: {fs}")
				self.error(f'Evaluation of {name} resulted in "{str(e)}" at line {str(fs.lineno)}')
		else:
			self.error(f'File {name} does not exist, cannot go on...')


	def perform_test_cases(self):
		global test_cases, mode
		self.hidden(f'This is vpl_evaluate.py version {self.version}')
		if mode.get("local"):
			self.hidden("*** Running in local mode ***")
		if mode.get("debug"):
			self.hidden("*** Running in debug mode ***")
		self.debug("VPL_SUBFILES :", os.environ.get('VPL_SUBFILES'))

		self.debug("test_cases =\n", self.pprinter.pformat(test_cases))

		score = 0
		total = 0
		total_reduction = 0
		min_grade = float(os.environ.get("VPL_GRADEMIN") or 0)
		max_grade = float(os.environ.get("VPL_GRADEMAX") or 100)
		directories = {}
		if test_cases.get('separate_directory'):
			for name in [ 'program', 'solution' ]:
				tmpdir = self.tempfile.mkdtemp(dir=self.tempfile.gettempdir())
				copy_script=f"cp -r $(ls -d * .[!.]* 2>/dev/null) '{tmpdir}'"
				copy_result = self.run_special("Global", "files_copy", copy_script, {}, 0)
				if copy_result.get('failure'):
					self.dispatch_error("*** Could not copy files to temporary directory", copy_result, 0)
				directories[name] = tmpdir

		if test_cases.get('sorted'):
			keys = sorted(test_cases.keys())
		else:
			keys = test_cases.keys()
		for name in keys:
			if type(test_cases[name]) is not dict or 'tests' not in test_cases[name]:
				continue
			case = test_cases | test_cases[name]
			if "variation" in case:
				if os.environ['VPL_VARIATION'] != case['variation']:
					continue
			weight = case.get('case_weight', 1)

			case_title = "Test case " + name
			if 'case_weight' in case:
				case_title += " (weight " + str(weight) + ")"
			self.title(case_title)

			# Tests
			os.environ['TESTCASE'] = name
			(test_score, points_reduction, percent_reduction) = self.perform_tests(name, case, directories)
			self.comment("=> Score for " + name + " : " + str(test_score * 100) + " / 100")
			score += test_score * weight
			total += weight
			total_reduction += points_reduction + (max_grade*percent_reduction)/100

		# remove temporary directories if any
		if test_cases.get('separate_directory') and not mode.get('keep_files'):
			for key in directories.keys():
				directory=directories[key]
				self.debug(f"Removing temporary directory {directory}")
				remove_result = self.run_special("Global", 'remove', f"rm -r '{directory}'", {}, False)
				if remove_result.get('failure'):
					self.dispatch_error("*** Could not remove temporary directory", remove_result, False)
		# remove cached executables
		if not mode.get('keep_files'):
			for file in self.executables_list:
				self.debug(f"Removing cached executable {file}")
				directory = os.path.dirname(file)
				os.remove(file)
				# remove any empty directory on the path
				try:
					os.removedirs(directory)
				except Exception:
					pass

		if total == 0:
			self.error("No test defined !")
		final_grade = score * float(max_grade) / total + float(min_grade)
		final_grade = max(final_grade - total_reduction, min_grade)
		self.grade(final_grade)


def read_and_remove(name):
	global iVPL
	return iVPL.read_and_remove(name)


def get_files(*unused, required=[], solution=[]):
	global iVPL
	return iVPL.get_files(required=required, solution=solution)


mode = {}
iVPL = IVPL()
test_cases = {}

if __name__ == '__main__':
	# read local setup and testcases to run
	iVPL.evaluate_file("vpl_evaluate.cases")
	# perform tests
	iVPL.perform_test_cases()
